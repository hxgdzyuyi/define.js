module('define');

define.ns('Ark')

test( 'mod可以正常挂载到匿名空间', function() {
  define.config({
    'mod/test': 'Ark.test'
  })

  define('mod/test', function(){
    return 23
  })

  ok( Ark.test == 23, 'Passed!' );
});

test( '带依赖的mod可以正常工作' , function(){
  define.config({
    'mod/jquery': 'Ark.jquery'
  , 'mod/dollar': 'Ark.dollar'
  , 'mod/dollar2': 'Ark.dollar2'
  })

  define('mod/jquery', function(){
    var $ = {}
    $.info = 23
    return $
  })

  define('mod/dollar', ['mod/jquery'], function(jquery){
    var dollar = jquery.info
    return dollar
  })

  define('mod/dollar2', ['mod/dollar', 'null'], function($){
    var dollar = $
    return dollar
  })

  ok( Ark.dollar2 == 23, 'Passed!' );
})

test( 'Require函数可以正常工作', function(){
  define.config({
    'mod/r': 'Ark.r'
  })

  define('mod/r', function(){
    return 23
  })

  require(['mod/r'], function(r){
    Ark.r = r
  })

  ok( Ark.r == 23, 'Passed!' );
})

test( '单个模块的require', function(){
  define.config({
    'mod/j': 'Ark.j'
  , 'mod/r': 'Ark.r'
  })

  define('mod/j', function(){
    return 'j'
  })

  var modJ = require('mod/j')

  ok( modJ === 'j', 'Passed!' );
})

asyncTest( 'require存在define函数中', function(){
  define.config({
    'mod/j': 'Ark.j'
  , 'mod/r': 'Ark.r'
  })

  var isRequire = 0
  define('mod/j', function(){
    return 'j'
  })

  define('mod/r', function(){
    var fn =  function($){
      isRequire = 1
      ok( $ === 'j', 'Passed!' );
    }
    start()
    require(['mod/j'], fn)
    require('mod/j', fn)
  })
})
